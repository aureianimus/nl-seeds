#!/bin/sh

for image in croppedframes/selectedframes/*.png
do
    target_file=$(basename $image)
    if [ ! -f ocr/tmp ]; then
        tesseract $image ocr/$target_file.tesseract -l eng --dpi 300 --oem 1 --psm 12
        gocr $image > ocr/$target_file.gocr
    fi
 
done
