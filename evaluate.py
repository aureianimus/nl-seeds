#!/usr/bin/python

import matplotlib.pyplot as plt
import numpy as np
import os
import PIL
import tensorflow as tf
import sys

from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.models import Sequential

import tempfile
import shutil

img_height = 1920
img_width = 1080

checkpoint_path = "cp.ckpt"
class_names= ['NO', 'YES']

num_classes = 2

model = Sequential([
  layers.experimental.preprocessing.Rescaling(1./255, input_shape=(img_height, img_width, 3)),
  layers.Conv2D(16, 3, padding='same', activation='relu'),
  layers.MaxPooling2D(),
  layers.Conv2D(32, 3, padding='same', activation='relu'),
  layers.MaxPooling2D(),
  layers.Conv2D(64, 3, padding='same', activation='relu'),
  layers.MaxPooling2D(),
  layers.Flatten(),
  layers.Dense(128, activation='relu'),
  layers.Dense(num_classes)
])


model.compile(optimizer='adam',
              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])

# Loads the weights
model.load_weights(checkpoint_path)

for file in os.listdir("./afterbirthplus"):
    selected_frame_path = "./selectedframes/" + file + ".png"
    if os.path.isfile(selected_frame_path):
        continue
    dirpath = tempfile.mkdtemp()
    #print(dirpath)
    command = 'ffmpeg -i "./afterbirthplus/' + file + '" -vf thumbnail=120,setpts=N/TB -r 1 -vframes 15 "' + dirpath + '/inputframes.%03d.png"'
    os.system(command)
    for frame in os.listdir(dirpath):
        frame_path = dirpath + "/" + frame
        
        img = keras.preprocessing.image.load_img(frame_path, target_size=(img_height, img_width))
        img_array = keras.preprocessing.image.img_to_array(img)
        img_array = tf.expand_dims(img_array, 0) # Create a batch

        predictions = model.predict(img_array)
        score = tf.nn.softmax(predictions[0])


        #print("This image ({}) most likely belongs to {} with a {:.2f} percent confidence.".format(frame_path, class_names[np.argmax(score)], 100 * np.max(score)))


        if class_names[np.argmax(score)] == "YES":
            os.system("mv " + frame_path + " " + selected_frame_path)
            break
    if class_names[np.argmax(score)] == "YES":
        shutil.rmtree(dirpath)
    else:
        os.system("echo " + dirpath + " >> failed_frames")

