#!/bin/sh

for image in selectedframes/*.png
do
    if [ ! -f croppedframes/$image ]; then
        convert "$image" -crop 225x155+300+225 -brightness-contrast 0x100 -set colorspace Gray "croppedframes/$image"
    fi
 
done
