#!/bin/sh

i="2"
while [ $i -lt 11 ]
do
    echo "$i"
    ffmpeg -i "$i.mp4" -vf thumbnail=120,setpts=N/TB -r 1 -vframes 15 "trainingframes/$i.%03d.png"

    i=$[$i+2]
done
