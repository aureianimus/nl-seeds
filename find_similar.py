#!/usr/bin/python

import glob


seeds = []

for filename in glob.glob("./ocr/*.tesseract.txt"):
    ocr_text = ""
    with open(filename, "r") as f:
        for line in f:
            ocr_text += line.strip().upper()

    seeds.append((filename, ocr_text))


matches = {}

for j in range(len(seeds)):
    for k in range(len(seeds)):
        if j < k:
            seed = seeds[j]
            seed2 = seeds[k]
            if len(seed[1]) == len(seed2[1]):
                match_number = 0
                for i in range(len(seed[1])):
                    if seed[1][i] == seed2[1][i]:
                        match_number += 1
                try:
                    matches[match_number]
                except:
                    matches[match_number] = []

                if match_number > 0:
                    matches[match_number].append((seed, seed2))

print(matches[5])
print(matches.keys())
